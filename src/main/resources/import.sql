INSERT INTO `roles` (id,nombre) VALUES ('1','ROLE_ADMIN');
INSERT INTO `roles` (id,nombre) VALUES ('2','ROLE_USER');

INSERT INTO `users` (username, password) VALUES ('admin','$2a$04$n6WIRDQlIByVFi.5rtQwEOTAzpzLPzIIG/O6quaxRKY2LlIHG8uty');
INSERT INTO `users` (username, password) VALUES ('john','$2a$04$n6WIRDQlIByVFi.5rtQwEOTAzpzLPzIIG/O6quaxRKY2LlIHG8uty');

INSERT INTO `users_roles` (usuario_id, role_id) VALUES ('admin', 1);
INSERT INTO `users_roles` (usuario_id, role_id) VALUES ('admin', 2);
INSERT INTO `users_roles` (usuario_id, role_id) VALUES ('john', 2);

INSERT INTO `books` (`id`, `title`, `stock`, `price`, `cover_page`) VALUES ('1', 'Harry Potter y la cámara secreta', '10', '35000', '1.png');
INSERT INTO `books` (`id`, `title`, `stock`, `price`, `cover_page`) VALUES ('2', 'Harry Potter y el prisionero de Azkaban', '8', '85000', '2.png');
INSERT INTO `books` (`id`, `title`, `stock`, `price`, `cover_page`) VALUES ('3', 'Harry Potter y el misterio del príncipe', '0', '45000', '3.png');

INSERT INTO `shopping_card` (`id`,`user_username`) VALUES ('1','admin');
INSERT INTO `item_shopping_cart` (`id`, `quantity`, `book_id` ,`item_shopping_cart_id`) VALUES ('1', '2', '1','1');

