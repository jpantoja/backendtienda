package com.johnpantoja.service;

import java.util.List;

import com.johnpantoja.entity.ShoppingCart;



public interface IShoppingCartService {

	public ShoppingCart saveShoppingCart(ShoppingCart shoppingCart) ;
	public List<ShoppingCart> findAll();
	
	public ShoppingCart findFacturaById(Long id);

	
}
