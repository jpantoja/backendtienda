package com.johnpantoja.service;

import com.johnpantoja.entity.User;

public interface IUserService {

	public User findByUsername(String username);
}
