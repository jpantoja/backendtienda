package com.johnpantoja.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.johnpantoja.dao.IShoppingCartDao;
import com.johnpantoja.entity.ShoppingCart;
@Service
public class ShoppingCartServiceImpl implements IShoppingCartService {

	
	
	
	
	
	@Autowired
	private IShoppingCartDao shoppingCartDaoDao;

	

	@Override
	@Transactional
	public ShoppingCart saveShoppingCart(ShoppingCart shoppingCart) {
		return shoppingCartDaoDao.save(shoppingCart);
	}

	
	
	@Override
	@Transactional(readOnly = true)
	public List<ShoppingCart> findAll() {
		return (List<ShoppingCart>) shoppingCartDaoDao.findAll();
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public ShoppingCart findFacturaById(Long id) {
		return shoppingCartDaoDao.findById(id).orElse(null);
	}
	
	

}
