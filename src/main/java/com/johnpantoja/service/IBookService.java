package com.johnpantoja.service;

import java.util.List;

import com.johnpantoja.entity.Book;
import com.johnpantoja.entity.ShoppingCart;



public interface IBookService {

	
	public List<Book> findAll();

	public Book saveBook(Book book) ;
	
	public Book findById(Long id);
}
