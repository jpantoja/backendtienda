package com.johnpantoja.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.johnpantoja.dao.IBookDao;
import com.johnpantoja.entity.Book;

@Service
public class BookServiceImpl implements IBookService {

	
	@Autowired
	private IBookDao bookDao;

	@Override
	@Transactional(readOnly = true)
	public List<Book> findAll() {
		return (List<Book>) bookDao.findAll();
	}
	
	@Override
	@Transactional
	public Book saveBook(Book book) {
		return bookDao.save(book);
	}

	

	@Override
	@Transactional(readOnly = true)
	public Book findById(Long id) {
		return bookDao.findById(id).orElse(null);
	}

	



}
