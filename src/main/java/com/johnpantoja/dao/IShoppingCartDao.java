package com.johnpantoja.dao;

import org.springframework.data.jpa.repository.JpaRepository;


import com.johnpantoja.entity.ShoppingCart;

public interface IShoppingCartDao extends JpaRepository<ShoppingCart, Long> {

}
