package com.johnpantoja.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.johnpantoja.entity.Book;

public interface IBookDao extends JpaRepository<Book, Long> {

}
