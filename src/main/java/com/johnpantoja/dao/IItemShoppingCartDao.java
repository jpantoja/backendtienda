package com.johnpantoja.dao;

import org.springframework.data.repository.CrudRepository;

import com.johnpantoja.entity.ItemShoppingCart;

public interface IItemShoppingCartDao extends CrudRepository<ItemShoppingCart, Long> {

}
