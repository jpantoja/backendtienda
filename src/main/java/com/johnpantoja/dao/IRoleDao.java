package com.johnpantoja.dao;

import org.springframework.data.repository.CrudRepository;

import com.johnpantoja.entity.Role;

public interface IRoleDao extends CrudRepository<Role, Long> {

}
