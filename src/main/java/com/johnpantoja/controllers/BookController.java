package com.johnpantoja.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.johnpantoja.entity.Book;
import com.johnpantoja.entity.ShoppingCart;
import com.johnpantoja.service.IBookService;
import com.johnpantoja.service.IShoppingCartService;

@CrossOrigin(origins = { "http://localhost:4200", "*" })
@RestController
@RequestMapping("/api")
public class BookController {

	@Autowired
	private IBookService bookService;

	@Autowired
	private IShoppingCartService shoppingCartService;

	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@GetMapping("/books")
	public List<Book> index() {
		return bookService.findAll();
	}

	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@PostMapping("/shoppingCart")
	@ResponseStatus(HttpStatus.CREATED)
	public ShoppingCart crear(@RequestBody ShoppingCart shoppingCart) {

		for (int i = 0; i < shoppingCart.getItemsShoppingCart().size(); i++) {

			int stock = shoppingCart.getItemsShoppingCart().get(i).getBook().getStock();
			int quantity = shoppingCart.getItemsShoppingCart().get(i).getQuantity();

			Book temp = bookService.findById(shoppingCart.getItemsShoppingCart().get(i).getBook().getId());
			temp.setStock(stock - quantity);
			bookService.saveBook(temp);

		}

		return shoppingCartService.saveShoppingCart(shoppingCart);
	}

	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@GetMapping("/shoppingCartAll")
	public List<ShoppingCart> getShoppingCartAll() {
		return shoppingCartService.findAll();
	}

	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@GetMapping("/shoppingCartAll/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ShoppingCart show(@PathVariable Long id) {
		return shoppingCartService.findFacturaById(id);
	}

}
